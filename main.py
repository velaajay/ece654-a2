import glob
import sys
from py2cfg import CFGBuilder, CFG
import ast


def load_test_files(input_directory="test") -> list:
    pattern = input_directory + "/*.py"
    result = glob.glob(pattern)
    return result


def get_values(assignment, results):
    """
    :param results:
    :param assignment: Will receive value for the assignment operator.
    :return: will return Unknown if the value can not be computed else a primitive value will be returned.
    """
    if isinstance(assignment, ast.BinOp):
        left = assignment.left.value if isinstance(assignment.left, ast.Constant) else get_values(
            assignment.left, results)
        right = assignment.right.value if isinstance(assignment.right, ast.Constant) else get_values(
            assignment.right, results)

        if left == "Unknown" or right == "Unknown" or not isinstance(left, int) or not isinstance(right, int):
            return "Unknown"

        if isinstance(assignment.op, ast.Add):
            return left + right
        elif isinstance(assignment.op, ast.Mult):
            return left * right
        elif isinstance(assignment.op, ast.Div):
            return left / right if right != 0 else "Unknown"
        elif isinstance(assignment.op, ast.Sub):
            return left - right
        elif isinstance(assignment.op, ast.Mod):
            return left % right
        else:
            return "Unknown"
    elif isinstance(assignment, ast.Constant):
        return assignment.value
    elif isinstance(assignment, ast.Name):
        # If the variable is previously known, you check in the dictionary and return the value.
        if assignment.id in results:
            return results[assignment.id]
        # Else the variable is "Unknown"
        else:
            return "Unknown"
    else:
        return "Unknown"


def values_from_op(statement, results, variable_states):
    # Check if the current operation is assignment.
    if isinstance(statement, ast.Assign):
        operator = statement.value
        value = get_values(operator, results)
        # In python the assignment could happen in a form like a, b = (Binary operation expression)
        for target in statement.targets:
            # Trying to maintain the history of the variable before doing an update in the values.
            if target.id in variable_states:
                # print("Appending for {0}".format(target.id))
                variable_states[target.id].append(results[target.id])
            else:
                # print("Init for {0}".format(target.id))
                variable_states[target.id] = list()
            results[target.id] = value


def get_values_for_comparison(operand, results):
    value = "Unknown"
    # Lets compute all the possible values for left.
    if isinstance(operand, ast.BinOp):
        value = get_values(operand, results)
    elif isinstance(operand, ast.Constant):
        value = operand.value
    elif isinstance(operand, ast.Name):
        if operand.id in results:
            value = results[operand.id]

    return value


def verify_if_test(stmt, results):
    test = stmt
    if isinstance(stmt, ast.If) or isinstance(stmt, ast.While):
        test = stmt.test

    if isinstance(test, ast.Compare):
        left = get_values_for_comparison(test.left, results)
        right = get_values_for_comparison(test.comparators[0], results)
        if left == "Unknown" or right == "Unknown":
            return False

        if isinstance(test.ops[0], ast.Eq):
            return left == right
        elif isinstance(test.ops[0], ast.NotEq):
            return left != right
        elif isinstance(test.ops[0], ast.Lt):
            return left < right
        elif isinstance(test.ops[0], ast.LtE):
            return left <= right
        elif isinstance(test.ops[0], ast.Gt):
            return left > right
        elif isinstance(test.ops[0], ast.GtE):
            return left >= right
        elif isinstance(test.ops[0], ast.Is):
            return left is right
        elif isinstance(test.ops[0], ast.IsNot):
            return left is not right
        elif isinstance(test.ops[0], ast.In):
            return left in right
        elif isinstance(test.ops[0], ast.NotIn):
            return left not in right
    elif isinstance(test, ast.Constant):
        return test.value is True
    elif isinstance(test, ast.BoolOp):
        result = False
        if isinstance(test.op, ast.And):
            result = True
            for value in test.values:
                result = result and verify_if_test(value, results)
                if not result:
                    return result
        elif isinstance(test.op, ast.Or):
            for value in test.values:
                result = result or verify_if_test(value, results)
                if result:
                    return result

        return result


def get_the_correct_path(links, results, queue_block):
    if verify_if_test(links[0].source.statements[0], results):
        queue_block.append(links[0].target)
    else:
        queue_block.append(links[1].target)


def collect_cfg_data(cfg: CFG):
    queue_block = [cfg.entryblock]
    results = dict()
    variable_states = dict()
    while queue_block:
        current_block = queue_block.pop(0)

        # Lets find all the exit blocks for the current block and add them to the queue.
        # More than one possible links, lets find the winning route.
        # print((isinstance(current_block.exits[0].source.statements[0], ast.While)))
        if len(current_block.exits) > 1:
            get_the_correct_path(current_block.exits, results, queue_block)
        elif len(current_block.exits) == 1:
            should_add_target = True
            # Below clause is added in case while block or if statement are the ending block.
            # This was going in an infinite loop.
            if (isinstance(current_block.exits[0].source.statements[0], ast.While) or isinstance(
                    current_block.exits[0].source.statements[0], ast.If)) and \
                    not verify_if_test(current_block.exits[0].source.statements[0], results):
                should_add_target = False
            if should_add_target:
                queue_block.append(current_block.exits[0].target)

        for stmt in current_block.statements:
            # Checking if this is an assignment operation
            if isinstance(stmt, ast.Assign):
                values_from_op(stmt, results, variable_states)

    return variable_states, results


def make_it_preety(variable_states, results):
    from prettytable import PrettyTable
    table = PrettyTable(border=True, header=True, padding_width=1,
                        field_names=["Variable Name", "Value", "State"])
    for key, value in results.items():
        for history in variable_states[key]:
            row = [key, history, ""]
            table.add_row(row)
        row = None

        if isinstance(value, int):
            row = [str(key), str(value) + " => (Final state)", "even" if value % 2 == 0 else "odd"]
        else:
            row = [key, str(value) + " => (Final state)", "Na"]

        table.add_row(row)

    print(table)


def report(python_source_file):
    if not python_source_file:
        print("Python source file can not be null", file=sys.stderr)
    try:
        name_from_file = python_source_file.split(".")[0]
        cfg = CFGBuilder().build_from_file(name_from_file, python_source_file)
        variable_states, results = collect_cfg_data(cfg)
        print("\nVariables analysis for {0} ".format(python_source_file))
        make_it_preety(variable_states, results)
        # cfg.build_visual(name_from_file, 'pdf')
    except FileNotFoundError as e:
        print("Can not open python source file {0}".format(python_source_file))
        sys.exit(1)


if __name__ == '__main__':
    for test_file in load_test_files():
        report(test_file)
