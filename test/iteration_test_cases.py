
loop_variable = 10
while loop_variable > 1:
    loop_variable = loop_variable - 1

break_stmt_variable = 1
while break_stmt_variable != 10:
    if break_stmt_variable % 2 == 0:
        break
    break_stmt_variable = break_stmt_variable + 1

