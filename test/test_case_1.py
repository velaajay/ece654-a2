"""
This file has only the test cases where only operations allowed on the variables
are assignment and arithmetic operations.
"""

first_variable = 2
second_variable = first_variable * 9

first = "Ajay"
last = "Kumar"
wat_iam = "a289kuma"
student = 20875438
email = "a289kumar@uwaterloo.ca"
hours = 35

# Lets increase the hours spent by five.
hours = hours + 5
# Double the hours
hours = hours * 2

# Trying multiplication operation on string (In python str * 2 = strstr
wat_iam = wat_iam * 2

# This should not change the variable state because there is no assignment operation.
third_variable = first_variable * 3
another_variable = 33


"""
Trying non-primitive variables, currently parity of those can not be detected so those are marked unknown.
"""

import random
object_1 = random.Random()

i_am_a_list = list()
i_am_a_list = [2,3,4,5,6]

i_am_a_map = {"Name": "Value"}

variable_run_time = input("Please enter a keyboard input")
