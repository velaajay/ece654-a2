"""
In this file we will include the test cases where variable values could be updated
by if-else blocks or conditionally.
"""

first_variable = 10
second_variable = 3
third_variable = "Init String"
deciding_variable = first_variable * 2

# A block where path goes to if block.
if deciding_variable % 2 == 0 and first_variable >= 10:
    first_variable = first_variable - 5
else:
    first_variable = first_variable + 5

# A block where path goes to else block.
if deciding_variable < 5:
    second_variable = second_variable / 5
else:
    second_variable = second_variable * 5


# A block where variable state was updated using else if block.
if deciding_variable < 5:
    third_variable = "Flow won't come here"
elif deciding_variable > 5:
    third_variable = "Update"
else:
    third_variable = "Flow won't come here."


# Lets try to hit some false positives. Here the output will depends on some unknown variables.

run_time_input = input("Please enter an input from key board")
false_positive = None
# Here we choose the else path because we were not able to find whether if clause worked.
if run_time_input > 10:
    false_positive = 5
else:
    false_positive = 20


if run_time_input > 10:
    false_positive = 5
elif false_positive is None:
    false_positive = 5
elif run_time_input == 10:
    false_positive = 10

