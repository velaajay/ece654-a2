class PrimitiveState:
    def __init__(self, name, expression):
        self.name = name
        self.expression = expression

    def update_expression(self, expression):
        self.expression = expression

    def __str__(self):
        return "Variable {0} -> {1}".format(self.name, self.expression)